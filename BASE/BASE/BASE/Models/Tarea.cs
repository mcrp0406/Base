﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace BASE.Models
{
    class Tarea
    {
        [PrimaryKey, AutoIncrement, Column("_id")]
        public int Id { get; set; }

        [MaxLength(150)]
        public string Name { get; set; }


        public DateTime DateFinish  { get; set; }

        public TimeSpan HourFinish { get; set; }

        public Boolean Completed { get; set; }
    }
}
