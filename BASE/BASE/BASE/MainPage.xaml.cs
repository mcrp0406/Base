﻿using BASE.Models; 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BASE
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}
       
        public void CreateTask(object sender, EventArgs e)
        {
            //Crear objeto del modelo Tarea 
            Tarea tarea = new Tarea() {
                Name = name.Text,
                DateFinish = dateFinish.Date,
                HourFinish = timeFinish.Time,
                Completed = false
            };
            // para que no se dupliquen las variables
            // creo conexion con la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                //Crear una tabla en base de datos  
                connection.CreateTable<Tarea>();

                //Crear un registro en la tabla "Tarea"
                var result = connection.Insert(tarea);

                if (result>0)
                {
                    DisplayAlert("Correcto","La Tarea se Creo Correctamente","Ok");
                }
                else
                {
                    DisplayAlert("Error", "La Tarea  no fue creada", "Ok");
                }
            };

           


        }

        async private void ListTask(object sender, EventArgs e)
            {
                   await Navigation.PushAsync(new ListTask());
            }
    } 
}
